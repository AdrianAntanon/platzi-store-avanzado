// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_api: 'https://platzi-store.herokuapp.com',
  firebase: {
    apiKey: 'AIzaSyAffbVkvkoXPhxatVYUJTbnUaJk7UEyuRU',
    authDomain: 'platzi-store-afd31.firebaseapp.com',
    databaseURL: 'https://platzi-store-afd31.firebaseio.com',
    projectId: 'platzi-store-afd31',
    storageBucket: 'platzi-store-afd31.appspot.com',
    messagingSenderId: '683141450861',
    appId: '1:683141450861:web:02915fb28841a1e08f8364',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
