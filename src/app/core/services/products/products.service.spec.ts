import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

import { ProductsService } from './products.service';

describe('ProductsService', () => {
  let service: ProductsService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ProductsService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('tests for getAllProducts', () => {
    it('should return products', () => {
      // Arrange o preparar
      const expectData = [
        {
          id: '1',
          image: 'img/img.jpg',
          title: 'test prueba',
          price: 100,
          description: 'test prueba',
        },
        {
          id: '2',
          image: 'img/img.jpg',
          title: 'test prueba',
          price: 100,
          description: 'test prueba',
        },
      ];
      let dataError;
      let dataResponse;
      // act o actuar
      service.getAllProducts().subscribe(
        (response) => {
          dataResponse = response;
        },
        (error) => {
          dataError = error;
        }
      );
      const req = httpTestingController.expectOne(
        `${environment.url_api}/products/`
      );
      req.flush(expectData);
      // assert o resolver nuestras hipótesis
      // expect(dataResponse.lenght).toEqual(2);
      expect(req.request.method).toEqual('GET');
      expect(dataError).toBeUndefined();
    });
  });
});


